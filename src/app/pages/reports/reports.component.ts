import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { IntegrationService } from '../integration/integration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  public reports = [];
  public loadingData = false;
  constructor(private fb: FormBuilder, private integrationService: IntegrationService, private router: Router) { }
  ngOnInit() {
    this.loadingData = true;
    this.integrationService.getReports().subscribe((resp)=>{
      this.loadingData = false;
      this.reports  = resp;
    })
  }

}
