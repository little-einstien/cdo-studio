
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IntegrationService } from '../../integration/integration.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public isConnecting = false;
  public connections = []
  public loadingData = false;
  validateForm!: FormGroup;
  dbDetailsForm!: FormGroup;
  isLoadingConnect = false;
  public dbList = [];
  submitForm(data): void {
    this.isLoadingConnect = true;
    this.integrationService.connectMysql(data).subscribe(response => {
      this.isLoadingConnect = false;
      this.integrationService.setConnectionDetails({ 'db': response, 'credentials': data, type: 'mysql' });
      this.router.navigate(['/connection-view']);
      this.dbList = response;
      // console.log(data);
      this.isLoadingConnect = false;
    });
    console.log(data);
  }
  constructor(private fb: FormBuilder, private integrationService: IntegrationService, private router: Router) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      fullname: [null, [Validators.required]],
      phone: [null, [Validators.required]],
    });
    this.getUsers();
  }
  host = '';
  username = '';
  current = 0;
  password = '';
  passwordVisible = 'password';
  index = 'First-content';
  collectionList = [];
  getCollectionList(db) {
    if (db) {
      console.log(db);
      let data = JSON.parse(localStorage.getItem('connectionDetails'));
      console.log(data);
      data['db'] = db;
      this.integrationService.getMongoCollectionList(data).subscribe((resp) => {
        console.log(resp);
        this.collectionList = resp;
      });
    }
  }
  getMongoData() { }
  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    // this.submitForm();
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'First-content';
        break;
      }
      case 1: {
        this.index = 'Second-content';
        break;
      }
      case 2: {
        this.index = 'third-content';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }
  public addConnection() {
    let data = {
      // id  : 1,
      full_name: this.validateForm.controls.fullname.value,
      email: this.validateForm.controls.email.value,
      phone: this.validateForm.controls.phone.value,

    }
    this.integrationService.addInternalUser(data).subscribe((resp) => {
      this.getUsers();
      this.close();
    });
  }
  public getUsers() {
    this.loadingData = true;
    this.integrationService.getUsers().subscribe((resp) => {
      console.log(resp);
      this.connections = resp;
      this.loadingData = false;
    });
  }
  visible = false;

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
  setUpdateForm(data) {
    this.validateForm.patchValue({
      fullname: data.fullname,
      email: data.email,
      phone: data.phone,
      
    });
    this.open();
  }
}
