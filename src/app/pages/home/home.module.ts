import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { HomeRoutingModule } from './home-routing.module';
import { IntegrationModule } from '../integration/integration.module';
import { ReportsComponent } from '../reports/reports.component';
import { UsersComponent } from './users/users.component';
import { EventsNotificationsComponent } from './events-notifications/events-notifications.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [HomeComponent, SideBarComponent, HeaderComponent, FooterComponent, ReportsComponent, UsersComponent, EventsNotificationsComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    HomeRoutingModule,
    IntegrationModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule
  ],
  providers:[{ provide: NZ_I18N, useValue: en_US }]
})
export class HomeModule { }
