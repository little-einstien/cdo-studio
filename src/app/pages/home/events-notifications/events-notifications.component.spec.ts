import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsNotificationsComponent } from './events-notifications.component';

describe('EventsNotificationsComponent', () => {
  let component: EventsNotificationsComponent;
  let fixture: ComponentFixture<EventsNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
