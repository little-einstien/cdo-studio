import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IntegrationService } from '../../integration/integration.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-events-notifications',
  templateUrl: './events-notifications.component.html',
  styleUrls: ['./events-notifications.component.scss']
})
export class EventsNotificationsComponent implements OnInit {

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '300px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  public isConnecting = false;
  public connections = []
  public users = []
  public loadingData = false;
  validateForm!: FormGroup;
  dbDetailsForm!: FormGroup;
  isLoadingConnect = false;
  public dbList = [];
  submitForm(data): void {
    this.isLoadingConnect = true;
    this.integrationService.connectMysql(data).subscribe(response => {
      this.isLoadingConnect = false;
      this.integrationService.setConnectionDetails({ 'db': response, 'credentials': data, type: 'mysql' });
      this.router.navigate(['/connection-view']);
      this.dbList = response;
      // console.log(data);
      this.isLoadingConnect = false;
    });
    console.log(data);
  }
  constructor(private fb: FormBuilder, private integrationService: IntegrationService, private router: Router) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      recipients: [null, [Validators.required]],
      body: [null, [Validators.required]],
    });
    this.getUsers();
    this.getNotifications();
  }
  host = '';
  username = '';
  current = 0;
  password = '';
  passwordVisible = 'password';
  index = 'First-content';
  collectionList = [];
  getCollectionList(db) {
    if (db) {
      console.log(db);
      let data = JSON.parse(localStorage.getItem('connectionDetails'));
      console.log(data);
      data['db'] = db;
      this.integrationService.getMongoCollectionList(data).subscribe((resp) => {
        console.log(resp);
        this.collectionList = resp;
      });
    }
  }
  getMongoData() { }
  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    // this.submitForm();
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'First-content';
        break;
      }
      case 1: {
        this.index = 'Second-content';
        break;
      }
      case 2: {
        this.index = 'third-content';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }
  public addConnection() {
    let data = {
      // id  : 1,
      name: this.validateForm.controls.name.value,
      recipients: this.validateForm.controls.recipients.value.join(','),
      body: this.validateForm.controls.body.value,
      
    }
    this.integrationService.addNotification(data).subscribe((resp) => {
      console.log(resp);
      // this.collectionList = resp;
      this.getNotifications();
      this.close();
    });
  }
  public getUsers() {
    this.integrationService.getUsers().subscribe((resp) => {
      console.log(resp);
      this.users = resp;
    });
  }

  public getNotifications() {
    this.loadingData = true;
    this.integrationService.getNotifications().subscribe((resp) => {
      console.log(resp);
      this.connections = resp;
      this.loadingData = false;
    });
  }
  visible = false;

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
  setUpdateForm(data) {
    this.validateForm.patchValue({
      name: data.name,
      recipients: data.recipients.split(','),
      body: data.body,
      
    });
    this.open();
  }
}

