import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { MongoComponent } from '../integration/mongo/mongo.component';
import { ConnectionViewComponent } from '../integration/connection-view/connection-view.component';
import { MysqlComponent } from '../integration/mysql/mysql.component';
import { ReportsComponent } from '../reports/reports.component';
import { EventsNotificationsComponent } from './events-notifications/events-notifications.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      {
        path: 'mongo',
        component: MongoComponent
      }, {
        path: 'mysql',
        component: MysqlComponent
      }, {
        path: 'reports',
        component: ReportsComponent
      },{
        path: 'users',
        component: UsersComponent
      }, {
        path: 'events',
        component: EventsNotificationsComponent
      }, 
    ],

  }, {
    path: 'connection-view',
    component: ConnectionViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
