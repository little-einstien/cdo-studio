import { Component, OnInit } from '@angular/core';
import { IntegrationService } from '../integration.service';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
// import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import { element } from 'protractor';
@Component({
  selector: 'app-connection-view',
  templateUrl: './connection-view.component.html',
  styleUrls: ['./connection-view.component.scss']
})
export class ConnectionViewComponent implements OnInit {
  validateForm!: FormGroup;
  isExportLoader = false;
  connectionDetails = {};
  collectionList = [];
  cols = [];
  dataLoading = false;
  scrollX = "100vw";
  scrollY = "240px";
  showFilters = false;
  public mongoData = null;
  public currentCol = '';
  public currentDB = '';
  public currentConnectionDetailsIndex = 0;
  public isSaveReportModalVisible = false;
  public reportName = "";
  public isAnalyticsVisible = false;
  public showGraph = false;
  public analyticsStarted = false;
  public paginate = true;
  public isExportDetailsVisible = false;
  public exportDB = [];
  public exportChildren = [];
  public exportCols = [];
  public isChildrenLoading = false;
  public mysqlAnalytics = null;
  public showMaskData = false;
  public maskingRequired = false;
  onCurrentPageDataChange($event) {

  }
  constructor(private fb: FormBuilder, private integrationService: IntegrationService, private router: Router, private notification: NzNotificationService) {
  }
  public filters = [];
  public transformer = [];
  public showTransformer = false;
  public analytics = {
    type: null,
    x: null,
    y: null
  };

  public destinationType = [
    {
      "label": "MySql",
      "value": 1
    },
    {
      "label": "Mongo DB",
      "value": 2
    },
    {
      "label": "AWS Dynamo DB",
      "value": 3
    },
    {
      "label": "Service Now",
      "value": 4
    },

  ]
  public analyticsType = [
    {
      "label": "Bar Chart",
      "value": 1
    },
    {
      "label": "Pie Chart",
      "value": 2
    },
    {
      "label": "Doughnut Chart",
      "value": 3
    },

  ]
  public transformOperationTypes = [
    {
      "label": "Add Fields (f1 + f2)",
      "value": 1
    },
    {
      "label": "Concat Fields (f1 + f2)",
      "value": 2
    },
    {
      "label": "Subtract Fields (f1 - f2)",
      "value": 3
    },
    {
      "label": "Multiply Fields (f1 * f2)",
      "value": 4
    },

  ];

  public overallAnalytics = [
    {
      name: 'Consistency',
      score: 0,
      threshold: 90
    },
    {
      name: 'Accuracy',
      score: 0,
      threshold: 90
    },
    {
      name: 'Completeness',
      score: 0,
      threshold: 92
    },
    {
      name: 'Auditability',
      score: 0,
      threshold: 80
    },
    {
      name: 'Orderliness',
      score: 0,
      threshold: 80
    },
    {
      name: 'Uniqueness',
      score: 0,
      threshold: 80
    },
    {
      name: 'Timeliness',
      score: 0,
      threshold: 80
    }
  ]
  public exportInfo = {
    name  :'',
    cols : []
  };
  public stats = [];
  ngOnInit() {
    this.connectionDetails = this.integrationService.connectioDetails.getValue();
    if (!this.connectionDetails) {
      this.router.navigate(['/mysql'])
    }
    this.validateForm = this.fb.group({
      host: [null, [Validators.required]],
      type: [null, []],
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      db: [null, []],
      collection: [null, []],
      maskingRequired: [null, []],
    });
  }
  getMySqlTables(i, isExport?) {
    if (!this.connectionDetails['db'][i].collections) {
      this.connectionDetails['db'][i].loader = true;
      let db = this.connectionDetails['db'][i];
      console.log(db);
      let data = this.connectionDetails['credentials'];
      data['db'] = db.db;
      this.integrationService.getMysqlTableList(data).subscribe((resp) => {
        console.log(resp);
        this.connectionDetails['db'][i].collections = resp;
        this.connectionDetails['db'][i].loader = false;
        if (isExport) {
          this.exportChildren = resp;
        }
        console.log(this.connectionDetails);
      });
    }
  }
  getDatabaseChildren(i, isExport?) {
    if (this.connectionDetails['type'] == 'mongo') {
      this.getCollectionList(i, isExport)
    } else if (this.connectionDetails['type'] == 'mysql') {
      this.getMySqlTables(i, isExport);
    }
  }
  getDatabaseChildrenForExport() {
    this.isExportLoader = true;
    this.isChildrenLoading = true;
    if (this.validateForm.controls.type.value == 1) {
      let db = this.validateForm.controls.db.value;
      console.log(db);
      let data = {
        'username': this.validateForm.controls.username.value,
        'host': this.validateForm.controls.host.value,
        'con_name': this.validateForm.controls.host.value,
        'password': this.validateForm.controls.password.value,
      }
      data['db'] = db;
      this.integrationService.getMysqlTableList(data).subscribe((resp) => {
        console.log(resp);
        this.exportChildren = resp;
        this.isExportLoader = false;
        this.isChildrenLoading = false;
      });
    } else if (this.validateForm.controls.type.value == 2) {
      let db = this.validateForm.controls.db.value;
      console.log(db);
      let data = {
        'username': this.validateForm.controls.username.value,
        'host': this.validateForm.controls.host.value,
        'con_name': this.validateForm.controls.host.value,
        'password': this.validateForm.controls.password.value,
      }
      data['db'] = db;
      this.integrationService.getMongoCollectionList(data).subscribe((resp) => {
        console.log(resp);
        this.exportChildren = resp;
        this.isExportLoader = false;
        this.isChildrenLoading = false;
      });
    }
  }
  getCollectionList(i, isExport?) {
    if (!this.connectionDetails['db'][i].collections) {
      this.connectionDetails['db'][i].loader = true;
      let db = this.connectionDetails['db'][i];
      console.log(db);
      let data = this.connectionDetails['credentials'];
      data['db'] = db.db;
      this.integrationService.getMongoCollectionList(data).subscribe((resp) => {
        console.log(resp);
        this.connectionDetails['db'][i].collections = resp;
        this.connectionDetails['db'][i].loader = false;
        if (isExport) {
          this.exportChildren = resp;
        }
        console.log(this.connectionDetails);
      });
    }
  }

  getData(i, collection, filters?, transformer?) {
    this.currentCol = collection;
    this.currentConnectionDetailsIndex = i;
    if (this.connectionDetails['type'] == 'mongo') {
      this.getMongoData(i, collection, filters, transformer);
    } else if (this.connectionDetails['type'] == 'mysql') {
      this.getMysqlData(i, collection, filters, transformer)
    }
  }
  getMongoData(i, collection, filters?, transformer?) {
    this.dataLoading = true;
    this.cols = []
    let db = this.connectionDetails['db'][i];
    let data = this.connectionDetails['credentials'];
    data['db'] = db.db;
    data['col'] = collection;
    this.currentDB = db.db;
    if (filters) {
      data['filters'] = filters
    }
    if (transformer) {
      data['transformer'] = transformer
    }
    this.integrationService.getMongoData(data).subscribe((resp) => {
      console.log(resp);
      this.dataLoading = false;
      this.mongoData = resp;
      for (var myKey in resp[0]) {
        console.log(myKey);
        this.cols.push(myKey);
      }
      data['filters'] = null;
    });
  }
  getMysqlData(i, collection, filters?, transformer?) {
    this.dataLoading = true;
    this.cols = []
    let db = this.connectionDetails['db'][i];
    let data = this.connectionDetails['credentials'];
    data['db'] = db.db;
    data['col'] = collection;
    this.currentDB = db.db;
    if (filters) {
      data['filters'] = filters
    }
    if (transformer) {
      data['transformer'] = transformer
    }
    this.integrationService.getMysqlData(data).subscribe((resp) => {
      console.log(resp);
      this.dataLoading = false;
      this.mongoData = resp;
      for (var myKey in resp[0]) {
        console.log(myKey);
        this.cols.push(myKey);
      }

      data['filters'] = null;
      this.getMySQLAnalytics();
    });
  }
  getMySQLAnalytics() {
    this.integrationService.getMysqlDataAnalytics({ data: this.mongoData, cols: this.cols }).subscribe((resp) => {
      console.log(resp);
      this.mysqlAnalytics = resp;
      let uniqueness = 0;
      let completeness = 0;
      this.mysqlAnalytics.forEach((element) => {
        uniqueness = uniqueness + (element.desc.unique  ? (element.desc.unique  / element.desc.count) * 100 : 1)
        this.getCoulmnStatics(element.col);
        let _temp = this.stats.filter(element => element.name == 'None')
        if (_temp.length) {
          completeness = completeness + ((element.desc.count - _temp[0].count) / element.desc.count) * 100;
        }else{
          completeness = completeness + 100;
        }
      })
      uniqueness = uniqueness / this.mysqlAnalytics.length;
      completeness = completeness / this.mysqlAnalytics.length;
      console.log("completeness", completeness);
      console.log("uniqueness", uniqueness);
      this.overallAnalytics[2]['score'] = Math.round(completeness);
      this.overallAnalytics[5]['score'] = Math.round(uniqueness);

    });
  }
  getCoulmnStatics(column) {
    var result = _(this.mongoData)
      .groupBy(column)
      .map(function (item, itemId) {
        let c = _.countBy(item, column);
        return { 'count': c[Object.keys(c)[0]], 'name': Object.keys(c)[0] }
      }).value();
    console.log(result);
    this.stats = result;
  }
  addFilter() {
    this.filters.push({
      field: null,
      condition: null,
      val: null
    });
  }
  removeFilter(index) {
    this.filters.splice(index, 1)
  }
  applyFilter() {
    console.log(this.filters);
    this.showFilters = false;
    this.getData(this.currentConnectionDetailsIndex, this.currentCol, this.filters)
  }
  addTransformation() {
    this.transformer.push({
      targets: null,
      op: null,
      result: null
    });
  }
  removeTransformation(index) {
    this.transformer.splice(index, 1)
  }
  applyTransformation() {
    console.log(this.transformer);
    this.showTransformer = false;
    this.getData(this.currentConnectionDetailsIndex, this.currentCol, this.filters, this.transformer)
    // this.getData(this.currentConnectionDetailsIndex, this.currentCol, this.filters)
  }
  resetFilter() {
    this.getData(this.currentConnectionDetailsIndex, this.currentCol);
  }
  handleCancel() {
    this.isSaveReportModalVisible = false;
  }
  handleOk() {
    let data = {}
    data['con_id'] = this.connectionDetails['credentials'].id;
    data['filters'] = this.filters
    data['name'] = this.reportName;
    this.isSaveReportModalVisible = false;
    this.integrationService.addReport(data).subscribe((resp) => {
      console.log(resp);
      this.createNotification('success', 'Success', 'Report has been saved !');
    });
  }
  createNotification(type: string, title, msg): void {
    this.notification.create(
      type,
      title,
      msg
    );
  }
  single: any[];
  multi: any[];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = this.analytics.x;
  showYAxisLabel = true;
  yAxisLabel = this.analytics.y;
  showLabels = true;
  isDoughnut = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  onSelect(event) {
    console.log(event);
  }
  analyticsTypeChanged($event) {

  }
  xAxisChanged($event) {

  }
  yAxisChanged($event) {
    this.analyticsStarted = true;
    let db = this.connectionDetails['db'][this.currentConnectionDetailsIndex];
    let data = this.connectionDetails['credentials'];
    data['db'] = db.db;
    data['col'] = this.currentCol;
    data['analytics'] = this.analytics;
    this.integrationService.getMysqlAnalytics(data).subscribe((resp) => {
      console.log(resp);
      this.single = resp;
      this.showGraph = true;
    })

  }
  goto(route) {
    this.router.navigate([route]);
  }
  exportConnect() {
    this.isExportLoader = true;
    if (this.validateForm.controls.type.value == 1) {
      this.integrationService.connectMysql({
        'username': this.validateForm.controls.username.value,
        'host': this.validateForm.controls.host.value,
        'con_name': this.validateForm.controls.host.value,
        'password': this.validateForm.controls.password.value,
      }).subscribe(response => {
        this.exportDB = response;
        this.isExportLoader = false;
      });
    } else if (this.validateForm.controls.type.value == 2) {
      this.integrationService.connectMongo({
        'username': this.validateForm.controls.username.value,
        'host': this.validateForm.controls.host.value,
        'con_name': this.validateForm.controls.host.value,
        'password': this.validateForm.controls.password.value,
      }).subscribe(response => {
        this.exportDB = response;
        this.isExportLoader = false;
      });
    }

  }
  getMySQLCols() {
    if (this.validateForm.controls.type.value == 1) {
      this.isExportLoader = true;
      this.integrationService.getMysqlColList({
        'db': this.validateForm.controls.db.value,
        'username': this.validateForm.controls.username.value,
        'collection': this.validateForm.controls.collection.value,
        'host': this.validateForm.controls.host.value,
        'con_name': this.validateForm.controls.host.value,
        'password': this.validateForm.controls.password.value,
      }).subscribe(response => {
        this.exportCols = response;
        this.isExportLoader = false;
      });
    }
  }
  exportData() {
    this.isExportLoader = true;
    let data = {
      export_from: this.connectionDetails['credentials'],
      export_to: {
        'db': this.validateForm.controls.db.value,
        'username': this.validateForm.controls.username.value,
        'col': this.validateForm.controls.collection.value,
        'host': this.validateForm.controls.host.value,
        'con_name': this.validateForm.controls.host.value,
        'password': this.validateForm.controls.password.value,
        'export_info' : this.exportInfo
      }
    }
    data.export_from['type'] = this.connectionDetails['type'] == 'mysql' ? 1 : 2;
    data.export_from['db'] = this.currentDB;
    data.export_from['col'] = this.currentCol;
    data.export_from['filters'] = this.filters;
    data.export_from['transformer'] = this.transformer;

    if (this.validateForm.controls.type.value == 2) {
      this.integrationService.exportDataToMongo(data).subscribe((resp) => {
        console.log(resp);
        if (resp['status'] == 1) {
          this.createNotification('success', 'Success', 'You Data has been exported Successfully!');
          this.showMaskData = false;
          this.isExportDetailsVisible = false;
          this.isExportLoader = false;
        }
      });
    }
  }
  export(){
    if(this.validateForm.controls.maskingRequired.value){
      this.showMaskData = true;
    }else{
      this.exportData();
    }
  }
}
