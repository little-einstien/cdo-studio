import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IntegrationService } from '../integration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-mysql',
  templateUrl: './mysql.component.html',
  styleUrls: ['./mysql.component.scss']
})
export class MysqlComponent implements OnInit {
  public isConnecting = false;
  public connections = []
  public loadingData = false;
  validateForm!: FormGroup;
  dbDetailsForm!: FormGroup;
  isLoadingConnect = false;
  public dbList = [];
  submitForm(data): void {
    this.isLoadingConnect = true;
    this.integrationService.connectMysql(data).subscribe(response => {
      this.isLoadingConnect = false;
      this.integrationService.setConnectionDetails({ 'db': response, 'credentials': data,type:'mysql' });
      this.router.navigate(['/connection-view']);
      this.dbList = response;
      // console.log(data);
      this.isLoadingConnect = false;
    });
    console.log(data);
  }
  constructor(private fb: FormBuilder, private integrationService: IntegrationService, private router: Router) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      host: [null, [Validators.required]],
      conname: [null, [Validators.required]],
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      // db: [null, []],
      // collection: [null, []],
    });
    this.getConnections();
  }
  host = '';
  username = '';
  current = 0;
  password = '';
  passwordVisible = 'password';
  index = 'First-content';
  collectionList = [];
  getCollectionList(db) {
    if (db) {
      console.log(db);
      let data = JSON.parse(localStorage.getItem('connectionDetails'));
      console.log(data);
      data['db'] = db;
      this.integrationService.getMongoCollectionList(data).subscribe((resp) => {
        console.log(resp);
        this.collectionList = resp;
      });
    }
  }
  getMongoData() { }
  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    // this.submitForm();
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'First-content';
        break;
      }
      case 1: {
        this.index = 'Second-content';
        break;
      }
      case 2: {
        this.index = 'third-content';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }
  public addConnection() {
    let data = {
      // id  : 1,
      conname: this.validateForm.controls.conname.value,
      host: this.validateForm.controls.host.value,
      username: this.validateForm.controls.username.value,
      password: this.validateForm.controls.password.value,
      type: 'mysql'
    }
    this.integrationService.addConnection(data).subscribe((resp) => {
      console.log(resp);
      // this.collectionList = resp;
      this.getConnections();
      this.close();
    });
  }
  public getConnections() {
    this.loadingData = true;
    this.integrationService.getConnections('mysql').subscribe((resp) => {
      console.log(resp);
      this.connections = resp;
      this.loadingData = false;
    });
  }
  visible = false;

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
  setUpdateForm(data){
    this.validateForm.patchValue({
      conname :data.con_name,
      host :data.host,
      username : data.username,
      password : data.password
    });
    this.open();
  }
}
