import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwsDynamoComponent } from './aws-dynamo.component';

describe('AwsDynamoComponent', () => {
  let component: AwsDynamoComponent;
  let fixture: ComponentFixture<AwsDynamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwsDynamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwsDynamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
