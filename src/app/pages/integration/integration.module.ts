import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MongoComponent } from './mongo/mongo.component';
import { ServiceNowComponent } from './service-now/service-now.component';
import { AwsDynamoComponent } from './aws-dynamo/aws-dynamo.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { ConnectionViewComponent } from './connection-view/connection-view.component';
import { MysqlComponent } from './mysql/mysql.component';
import { ChartsModule } from 'ng2-charts';

import { NgxChartsModule } from '@swimlane/ngx-charts';
@NgModule({
  declarations: [MongoComponent, ServiceNowComponent, AwsDynamoComponent, ConnectionViewComponent, MysqlComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule ,
    NgZorroAntdModule,
    ChartsModule,
    NgxChartsModule
    
  ]
})
export class IntegrationModule { }
