import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IntegrationService {
  connectioDetails: BehaviorSubject<any> = new BehaviorSubject(null);;
  setConnectionDetails(details) {
    this.connectioDetails.next(details);
  }

  constructor(private http: HttpClient) { }
  connectMongo(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mongo`, payload);
  }
  connectMysql(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mysql`, payload);
  }
  getMongoCollectionList(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mongo-collection-list`, payload);
  }
  getMysqlTableList(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mysql-table-list`, payload);
  }
  getMongoData(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mongodata`, payload);
  }
  getMysqlData(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mysqldata`, payload);
  }
  getMysqlDataAnalytics(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mysql-data-analytics`, payload);
  }
  getMysqlColList(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mysql-cols`, payload);
  }
  addConnection(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/add-connection`, payload);
  }
  addInternalUser(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/add-user`, payload);
  }
  addNotification(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/add-notification`, payload);
  }
  addReport(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/add-report`, payload);
  }
  getConnections(type) {
    return this.http.get<any>(`${environment.apiEndpoint}/get-connection-list?type=${type}`);
  }
  getUsers() {
    return this.http.get<any>(`${environment.apiEndpoint}/get-user-list`);
  }
  getNotifications() {
    return this.http.get<any>(`${environment.apiEndpoint}/get-notification-list`);
  }
  getReports() {
    return this.http.get<any>(`${environment.apiEndpoint}/get-report-list`);
  }
  login(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/login-new`, payload);
  }
  getMysqlAnalytics(payload) {
    return this.http.post<any>(`${environment.apiEndpoint}/mysql-analytics`, payload);
  }
  exportDataToMongo(payload){
    return this.http.post<any>(`${environment.apiEndpoint}/export-to-mongo`, payload);
  }
}
