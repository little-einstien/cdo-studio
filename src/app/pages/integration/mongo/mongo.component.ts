import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IntegrationService } from '../integration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-mongo',
  templateUrl: './mongo.component.html',
  styleUrls: ['./mongo.component.scss']
})
export class MongoComponent implements OnInit {
  public connections = []
  public loadingData = false;
  validateForm!: FormGroup;
  dbDetailsForm!: FormGroup;
  isLoadingConnect = false;
  public dbList = [];
  submitForm(data): void {
    this.isLoadingConnect = true;
    this.integrationService.connectMongo(data).subscribe(response => {
      this.isLoadingConnect = false;
      this.integrationService.setConnectionDetails({'db' : response,'credentials' :data ,type:'mongo' });
      this.router.navigate(['/connection-view']);
      this.dbList = response;
      // console.log(data);
    });;
    console.log(data);
  }
  constructor(private fb: FormBuilder, private integrationService: IntegrationService, private router: Router) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      host: [null, [Validators.required]],
      conname: [null, [Validators.required]],
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      // db: [null, []],
      // collection: [null, []],
    });
    this.getConnections();
  }
  host = '';
  username = '';
  current = 0;
  password = '';
  passwordVisible = 'password';
  index = 'First-content';
  collectionList = [];
  getCollectionList(db) {
    if (db) {
      console.log(db);
      let data = JSON.parse(localStorage.getItem('connectionDetails'));
      console.log(data);
      data['db'] = db;
      this.integrationService.getMongoCollectionList(data).subscribe((resp) => {
        console.log(resp);
        this.collectionList = resp;
      });
    }
  }
  getMongoData() { }
  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    // this.submitForm();
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'First-content';
        break;
      }
      case 1: {
        this.index = 'Second-content';
        break;
      }
      case 2: {
        this.index = 'third-content';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }
  public addConnection() {
    let data = {
      // id  : 1,
      conname: this.validateForm.controls.conname.value,
      host: this.validateForm.controls.host.value,
      username: this.validateForm.controls.username.value,
      password: this.validateForm.controls.password.value,
      type : 'mongo'
    }
    this.integrationService.addConnection(data).subscribe((resp) => {
      console.log(resp);
      // this.collectionList = resp;
      this.getConnections();
      this.close();
    });
  }
  public getConnections() {
    this.loadingData = true;
    this.integrationService.getConnections('mongo').subscribe((resp) => {
      console.log(resp);
      this.connections = resp;
      this.loadingData = false;
    });
  }
  visible = false;

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
  openExport(){
    
  }
}
