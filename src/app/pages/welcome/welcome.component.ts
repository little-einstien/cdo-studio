import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IntegrationService } from '../integration/integration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  validateForm!: FormGroup;

  constructor(private fb: FormBuilder, private integrationService: IntegrationService,private router:Router) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.valid) {
      let payload = {
        username: this.validateForm.controls.userName.value,
        password: this.validateForm.controls.password.value,
      };
      this.integrationService.login(payload).subscribe((result) => {
        if(result.status == 1){
          this.router.navigate(['/mysql'])
        }
      })
    }
  }
}
